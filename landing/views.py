from django.shortcuts import render


# Create your views here.
landing_page_content = ''


def index(request):
    response = {'content': landing_page_content}
    return render(request, 'index.html', response)
